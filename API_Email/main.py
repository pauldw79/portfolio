import os
from dotenv import load_dotenv
from weather import WeatherInfo
from send_email import EmailSender

load_dotenv()

def main():
    api_key = os.getenv("API_KEY")
    email_address = os.getenv("EMAIL_ADDRESS")
    email_password = os.getenv("EMAIL_PASSWORD")
    to_email = os.getenv("TO_EMAIL")
    subject = "Weather Forecast"

    city = os.getenv("CITY")
    state = os.getenv("STATE")
    country = "United States"

    # Create instances of WeatherInfo and EmailSender
    weather_info = WeatherInfo(api_key)
    email_sender = EmailSender()

    # Prepare weather information
    current_weather, forecast = weather_info.prepare_weather_info(city, state, country)

    # Prepare email content
    email_body = f"{current_weather}\n\n{forecast}\n"

    # Send the email
    email_sender.send_email(to_email, subject, email_body)

if __name__ == "__main__":
    main()

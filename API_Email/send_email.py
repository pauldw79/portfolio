import yagmail
from dotenv import load_dotenv
import os

load_dotenv()

class EmailSender:
    def __init__(self):
        self.email_address = os.getenv("EMAIL_ADDRESS")
        self.email_password = os.getenv("EMAIL_PASSWORD")
        self.server = None  # yagmail SMTP server instance

    def send_email(self, to_email, subject, body):
        try:
            # Initialize the yagmail SMTP server
            self.server = yagmail.SMTP(self.email_address, self.email_password)

            # Send the email
            self.server.send(to=to_email, subject=subject, contents=body)
            print("Email sent successfully!")
        except Exception as e:
            print(f"Error sending email: {e}")
        finally:
            # Close the server after sending or in case of an error
            self.disconnect()

    def disconnect(self):
        if self.server:
            try:
                self.server.close()
                print("Disconnected from yagmail SMTP server")
            except Exception as e:
                print(f"Error disconnecting from yagmail SMTP server: {e}")


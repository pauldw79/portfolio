import os
import datetime
import requests
from dotenv import load_dotenv

load_dotenv()

class WeatherAPI:
    def __init__(self, api_key, units='imperial'):
        self.api_key = api_key
        self.units = units

    def get_current_weather(self, city, state, country):
        base_url = "https://api.weatherapi.com/v1/current.json"
        params = {"key": self.api_key, "q": f"{city},{state},{country}", "units": self.units}
        return self._make_request(base_url, params)

    def get_weather_forecast(self, city, state, country, days=1):
        base_url = "https://api.weatherapi.com/v1/forecast.json"
        params = {"key": self.api_key, "q": f"{city},{state},{country}", "days": days, "units": self.units}
        return self._make_request(base_url, params)

    def _make_request(self, base_url, params):
        try:
            response = requests.get(base_url, params=params)
            response.raise_for_status()
            return response.json()
        except requests.exceptions.RequestException as e:
            return {"error": {"message": f"Request error: {e}"}}

class WeatherFormatter:
    emoji_mapping = {
        "Sunny": "☀️",
        "Clear": "☀️",
        "Partly cloudy": "⛅",
        "Cloudy": "☁️",
        "Overcast": "☁️",
        "Mist": "🌫️",
        "Fog": "🌁",
        "Light rain": "🌧️",
        "Moderate rain": "🌧️",
        "Heavy rain": "🌧️☔",
        "Light snow": "❄️",
        "Moderate snow": "❄️",
        "Heavy snow": "❄️",
        "Thunderstorm": "⛈️",
        "Patchy rain possible": "🌦️",
        "Patchy snow possible": "🌨️",
    }

    @staticmethod
    def format_current_weather(data):
        if "location" in data and "current" in data:
            location = data["location"]["name"]
            temperature = data["current"]["temp_f"]
            condition = data["current"]["condition"]["text"]

            emoji = WeatherFormatter.emoji_mapping.get(condition, "🌍")  # Accessing at the class level

            return f"Weather in {location}: {temperature}°F, {emoji}  {condition}"
        else:
            return "Weather data not available."

    @staticmethod
    def format_weather_forecast(data, forecast_days=3):
        if "forecast" in data and "forecastday" in data['forecast']:
            forecast_days = min(forecast_days, len(data['forecast']['forecastday']))
            forecast_strings = []
            for day in data['forecast']['forecastday'][:forecast_days]:
                date = (datetime.datetime.strptime(day['date'], '%Y-%m-%d') + datetime.timedelta(days=1)).strftime(
                    '%Y-%m-%d')
                max_temp = day['day']['maxtemp_f']
                min_temp = day['day']['mintemp_f']
                forecast_condition = day['day']['condition']['text']

                emoji = WeatherFormatter.emoji_mapping.get(forecast_condition, "🌍")

                forecast_strings.append(
                    f"{date}: Max Temp: {max_temp}°F, Min Temp: {min_temp}°F, {emoji}  {forecast_condition}")
            return "\n".join(forecast_strings)
        else:
            return "Forecast data not available."


class WeatherInfo:
    def __init__(self, api_key, units='imperial'):
        self.api_key = api_key
        self.units = units
        self.weather_api = WeatherAPI(api_key, units)
        self.weather_formatter = WeatherFormatter()

    def prepare_weather_info(self, city, state, country, forecast_days=3):
        current_weather_data = self.weather_api.get_current_weather(city, state, country)
        forecast_data = self.weather_api.get_weather_forecast(city, state, country, days=forecast_days)

        current_weather_str = self.weather_formatter.format_current_weather(current_weather_data)
        forecast_str = self.weather_formatter.format_weather_forecast(forecast_data)

        return current_weather_str, forecast_str

    def print_weather_info(self, city, state, country, forecast_days=3):
        current_weather_str, forecast_str = self.prepare_weather_info(city, state, country, forecast_days)
        print(current_weather_str)
        print("Three day forecast:")
        print(forecast_str)

"""Module for defining the Alien class and its behavior."""

import pygame

from pygame.sprite import Sprite


class Alien(Sprite):
    """A class representing an alien ship."""
    def __init__(self, ai_game):
        # Initialize the alien and set its starting position.
        super().__init__()
        self.screen = ai_game.screen
        self.settings = ai_game.settings

        # Load the alien image and set its rect attribute.
        self.image = pygame.image.load('Images/alien_ship.bmp')
        self.image = pygame.transform.scale(self.image, (50, 50))
        self.rect = self.image.get_rect()

        # Start each new alien near the top left of the screen.
        self.rect.x = self.rect.width
        self.rect.y = self.rect.height

        # Store the alien's exact horizontal position.
        self.x = float(self.rect.x)

        self.hit_sound = pygame.mixer.Sound(
            'Sounds/whoosh.flac')

    def update(self, *args, **kwargs):
        """Move the alien to the right or left."""
        self.x += (self.settings.alien_speed * self.settings.fleet_direction)
        self.rect.x = self.x

    def check_edges(self):
        """Return True if alien is at edge of screen."""
        screen_rect = self.screen.get_rect()
        if self.rect.right >= screen_rect.right or self.rect.left <= 0:
            return True

    def play_hit_sound(self):
        """Play the hit sound effect for the alien."""
        self.hit_sound.play()

"""
Alien Invasion Main Module

This module contains the main game logic and class for the Alien Invasion game.

Classes:
    AlienInvasion: The main class to manage game assets and behavior.
"""


import sys
import os
from time import sleep

import pygame

from settings import Settings
from game_stats import GameStats
from ship import Ship
from bullet import Bullet
from alien import Alien
from scoreboard import Scoreboard
from start_screen import StartButton, StartScreen
from sounds import Sounds
from end_screen import EndScreen
from db import db_config

# Set the working directory to the module's directory
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)


class AlienInvasion:
    """
    The main class to manage game assets and behavior.
    """

    def __init__(self):
        """
        Initialize the game, and create game resources.
        """
        pygame.init()  # Initialize pygame
        self.settings = Settings()

        self.screen = pygame.display.set_mode(
            (self.settings.screen_width, self.settings.screen_height))
        # self.screen = pygame.display.set_mode((0,0), pygame.FULLSCREEN)
        # self.settings.screen_width = self.screen.get_rect().width
        # self.settings.screen_height = self.screen.get_rect().height
        pygame.display.set_caption("Alien Invasion")

        self.stats = GameStats(self)

        # Initialize the current score
        self.current_score = 0

        self.scoreboard = Scoreboard(self)
        self.start_screen = StartScreen(self)
        self.end_screen = EndScreen(self, db_config)

        self.ship = Ship(self)
        self.bullets = pygame.sprite.Group()
        self.aliens = pygame.sprite.Group()

        self._create_fleet()

        # Make the Play button.
        self.play_button = StartButton(self, "Play")

        # Create an instance of the Sounds class
        self.sounds = Sounds()

        # Start the background music
        self.sounds.play()

    def run_game(self):
        """
        Start the main game loop.
        """
        start_screen = None
        end_screen = None

        # Flag to track if the high score has been saved
        high_score_saved = False

        # Start the main loop for the game
        while True:
            # Check for events
            self._check_events()

            # Check if the game is over (for example, when all ships are used)
            if self.stats.ships_left == 0 and not high_score_saved:
                # Save the high score to the database
                self.stats.game_active = False
                self.scoreboard.save_score_to_database()
                high_score_saved = True

                # Create a new instance of the end screen
                end_screen = EndScreen(self, db_config)

            # Display screens based on game state
            if not self.stats.game_active:
                if self.stats.ships_left > 0:
                    if start_screen is None:
                        start_screen = StartScreen(self)
                    start_screen.draw_screen()
                    pygame.mouse.set_visible(True)
                elif self.stats.ships_left == 0 and end_screen is not None:
                    # Display the end screen
                    end_screen.show_high_scores()
                    end_screen.draw_screen()
                    pygame.mouse.set_visible(True)
            else:
                # If the game is active, update the game elements and draw the
                # screen
                self.ship.update()
                self._update_bullets()
                self._update_aliens()
                self._update_screen()
                self.stats.game_started = True
                pygame.mouse.set_visible(False)

    def _update_bullets(self):
        """
        Update the position of bullets and get rid of old bullets.
        """
        self.bullets.update()
        # Get rid of bullets that have disappeared.
        for bullet in self.bullets.copy():
            if bullet.rect.bottom <= 0:
                self.bullets.remove(bullet)

        self._check_bullet_alien_collisions()

    def _check_bullet_alien_collisions(self):
        """
        Check for bullet-alien collisions and respond accordingly.
        """
        collisions = pygame.sprite.groupcollide(self.bullets, self.aliens,
                                                True, True)

        if collisions:
            for aliens in collisions.values():
                points_earned = self.settings.alien_points * len(aliens)
                self.stats.score += points_earned
            hit = Alien(self)
            hit.play_hit_sound()
            self.scoreboard.prep_score()

        if not self.aliens:
            # Destry existing bullets and create new fleet.
            self.bullets.empty()
            self._create_fleet()
            self.settings.increase_speed()

            # Increase level and change background color
            self.stats.level += 1
            self.settings.bg_color = self.settings._generate_random_color()
            self.scoreboard.prep_level()
            self.scoreboard.prep_score()
            self.scoreboard.prep_ships()
            self.scoreboard.update_best_score()

    def _update_aliens(self):
        """
        Update the positions of all aliens in the fleet.
        """
        self._check_fleet_edges()
        # Update the positions of all aliens in the fleet.
        self.aliens.update()
        # Look for alien-ship collisions.
        if pygame.sprite.spritecollideany(self.ship, self.aliens):
            self._ship_hit()
        # Look for aliens hitting the bottom of the screen.
        self._check_aliens_bottom()

    def _create_fleet(self):
        """
        Create the fleet of aliens.
        """
        alien = Alien(self)
        alien_width, alien_height = alien.rect.size
        alien_width = alien.rect.width
        available_space_x = self.settings.screen_width - (2 * alien_width)
        number_aliens_x = available_space_x // (2 * alien_width)

        # Determine the number of rows of aliens that fit on the screen.
        ship_height = self.ship.rect.height
        available_space_y = (self.settings.screen_height -
                             (3 * alien_height) - ship_height)
        number_rows = available_space_y // (2 * alien_height)

        # Create the full fleet of aliens.
        for row_number in range(number_rows):
            # Create the first row of aliens.
            for alien_number in range(number_aliens_x):
                self._create_alien(alien_number, row_number)

    def _create_alien(self, alien_number, row_number):
        """
        Create an alien and place it in the fleet.

        Args:
            alien_number (int): The position of the alien in the row.
            row_number (int): The row number of the alien.
        """
        alien = Alien(self)
        alien_width, alien_height = alien.rect.size
        alien_width = alien.rect.width
        alien.x = alien_width + 2 * alien_width * alien_number
        alien.rect.x = alien.x
        alien.rect.y = alien_height + 2 * alien.rect.height * row_number
        self.aliens.add(alien)

    def _check_fleet_edges(self):
        """
        Check if any aliens have reached an edge and respond accordingly.
        """
        for alien in self.aliens.sprites():
            if alien.check_edges():
                self._change_fleet_direction()
                break

    def _change_fleet_direction(self):
        """
        Change the fleet's direction and drop the entire fleet.
        """
        for alien in self.aliens.sprites():
            alien.rect.y += self.settings.fleet_drop_speed
        self.settings.fleet_direction *= -1

    def _check_events(self):
        """
        Check for and respond to events.
        """
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                sys.exit()
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                self._check_play_button(mouse_pos)
            elif event.type == pygame.KEYDOWN:
                self._check_keydown_events(event)
            elif event.type == pygame.KEYUP:
                self._check_keyup_events(event)

    def _check_play_button(self, mouse_pos):
        """
        Check if the play button has been clicked.

        Args:
            mouse_pos (tuple): The mouse position.
        """
        button_clicked = self.play_button.rect.collidepoint(mouse_pos)
        if button_clicked and not self.stats.game_active:
            # Reset the game settings.
            self.settings.initialize_dynamic_settings()
            self.stats.reset_stats()
            self.stats.game_active = True

            # Reset the scoreboard to its initial state.
            self.scoreboard.prep_score()
            self.scoreboard.prep_level()
            self.scoreboard.prep_ships()
            self.scoreboard.update_best_score()

            # Reset the background color to a new random color
            self.settings.bg_color = self.settings._generate_random_color()
            self.scoreboard.bg_color = self.settings.bg_color

            # Get rid of any remaining aliens and bullets.
            self.aliens.empty()
            self.bullets.empty()

            # Create a new fleet and center the ship.
            self._create_fleet()
            self.ship.center_ship()

    def _check_keydown_events(self, event):
        """
        Respond to keydown events.

        Args:
            event (pygame.event.Event): The keydown event.
        """
        if event.key == pygame.K_RIGHT:
            # move the ship to the right
            self.ship.moving_right = True
        elif event.key == pygame.K_LEFT:
            # move the ship to the left
            self.ship.moving_left = True
        elif event.key == pygame.K_q:
            sys.exit()
        elif event.key == pygame.K_SPACE:
            self._fire_bullet()

    def _check_keyup_events(self, event):
        """
        Respond to keyup events.

        Args:
            event (pygame.event.Event): The keyup event.
        """
        if event.key == pygame.K_RIGHT:
            self.ship.moving_right = False
        elif event.key == pygame.K_LEFT:
            self.ship.moving_left = False

    def _fire_bullet(self):
        """
        Fire a bullet from the ship.
        """
        if len(self.bullets) < self.settings.bullets_allowed:
            new_bullet = Bullet(self)
            new_bullet.play_sound()
            self.bullets.add(new_bullet)

    def _ship_hit(self):
        """
        Respond to the ship being hit by an alien.
        """
        if self.stats.ships_left > 0:
            # Decrement ships_left, and update scoreboard.
            self.stats.ships_left -= 1
            self.scoreboard.prep_ships()
            # Get rid of any remaining aliens and bullets.
            self.aliens.empty()
            self.bullets.empty()
            # Create a new fleet and center the ship.
            self._create_fleet()
            self.ship.center_ship()
            # Pause
            sleep(.5)
        else:
            self.stats.game_active = False
            pygame.mouse.set_visible(True)

    def _check_aliens_bottom(self):
        """
        Check if any aliens have reached the bottom of the screen.
        """
        screen_rect = self.screen.get_rect()
        for alien in self.aliens.sprites():
            if alien.rect.bottom >= screen_rect.bottom:
                # Treat this the same as if the ship got hit.
                self._ship_hit()
                break

    def _update_screen(self):
        """
        Update the game screen and draw game elements.
        """
        self.screen.fill(self.settings.bg_color)
        self.screen.blit(self.settings.city_image, self.settings.city_rect)

        for bullet in self.bullets.sprites():
            bullet.draw_bullet()
        self.aliens.draw(self.screen)

        # Draw the score information.
        self.scoreboard.show_score()

        # Draw the player-controlled ship on top of other ships.
        self.ship.blitme()

        # Draw the play button if the game is inactive.
        if not self.stats.game_active:
            self.play_button.draw_button()

        # Make the most recently drawn screen visible.
        pygame.display.flip()
        # Continuously update the game display.
        pygame.display.update()


if __name__ == "__main__":
    # Make a game instance, and run the game.
    ai = AlienInvasion()
    ai.run_game()

"""Module for defining the end screen of the game."""

import pygame.font
import psycopg2
from pygame.sprite import Sprite


class EndButton(Sprite):
    """A class to represent a button on the end screen."""

    def __init__(self, ai_game, msg):
        """Initialize button attributes.

        Args:
            ai_game (object): The main game object.
            msg (str): The message displayed on the button.
        """
        super().__init__()
        self.screen = ai_game.screen
        self.screen_rect = self.screen.get_rect()

        # Set the dimensions and properties of the button.
        self.width, self.height = 200, 50
        self.button_color = (0, 255, 0)
        self.text_color = (255, 255, 255)
        self.font = pygame.font.SysFont(None, 48)

        # Build the button's rect object and center it.
        self.rect = pygame.Rect(0, 0, self.width, self.height)
        self.rect.center = self.screen_rect.center
        self.rect.bottom = self.screen_rect.bottom - 150

        # The button message needs to be prepped only once.
        self._prep_msg(msg)

        # Initialize clicked attribute.
        self.clicked = False

    def _prep_msg(self, msg):
        """Turn msg into a rendered image and center text on the button.

        Args:
            msg (str): The message to display on the button.
        """
        self.msg_image = self.font.render(
            msg, True, self.text_color, self.button_color)
        self.msg_image_rect = self.msg_image.get_rect()
        self.msg_image_rect.center = self.rect.center

    def draw_button(self):
        """Draw the button to the screen."""
        self.screen.fill(self.button_color, self.rect)
        self.screen.blit(self.msg_image, self.msg_image_rect)


class EndScreen:
    """A class to represent the end screen of the game."""

    def __init__(self, ai_game, db_config):
        """Initialize end screen attributes.

        Args:
            ai_game (object): The main game object.
            db_config (dict): Database configuration parameters.
        """
        self.screen = ai_game.screen
        self.screen_rect = self.screen.get_rect()
        self.settings = ai_game.settings
        self.db_config = db_config
        self.score_strings = []  # Initialize score_strings attribute

        # Load the background image and scale it to fit the screen.
        self.background_image = pygame.image.load(
            'Images/end_bg.jpg').convert()
        self.background_image = pygame.transform.scale(
            self.background_image, self.screen_rect.size)

        self.title = "GAME OVER"
        self.font = pygame.font.SysFont(None, 100)
        self.title_image = pygame.Surface((500, 200), pygame.SRCALPHA)
        self.title_image.fill((255, 255, 255, 0))
        text = self.font.render(self.title, True, (255, 255, 255))
        self.title_image.blit(text, (0, 0))
        self.title_rect = self.title_image.get_rect()
        self.title_rect.centerx = self.screen_rect.centerx
        self.title_rect.y = self.screen_rect.top + 50

        high_scores_text = "High Scores"
        self.high_scores_font = pygame.font.SysFont(None, 36)
        self.high_scores_image = self.high_scores_font.render(
            high_scores_text, True, (255, 255, 255)
        )

        # Initialize the font for individual score entries
        self.score_font = pygame.font.SysFont(None, 30)

        self.high_scores_image_rect = self.high_scores_image.get_rect()
        self.high_scores_image_rect.centerx = self.title_rect.centerx
        self.high_scores_image_rect.y = self.title_rect.bottom + 2

        self.play_button = EndButton(ai_game, "Replay")

    def show_high_scores(self):
        """Retrieve and display the high scores from the database."""
        try:
            connection = psycopg2.connect(**self.db_config)
            cursor = connection.cursor()

            # Fetch the top 5 scores from the database
            select_query = (
                "SELECT score "
                "FROM high_scores "
                "ORDER BY score DESC "
                "LIMIT 5;"
            )
            cursor.execute(select_query)
            top_scores = cursor.fetchall()

            # Create a list of rendered score strings
            score_offset = self.high_scores_image_rect.bottom + 10

            for i, score in enumerate(top_scores):
                score_str = f"{i + 1:2d}. {score[0]:,}"
                score_image = self.score_font.render(
                    score_str, True, (255, 255, 255))
                score_rect = score_image.get_rect()
                score_rect.centerx = self.title_rect.centerx
                score_rect.y = score_offset
                self.score_strings.append((score_image, score_rect))
                score_offset += score_rect.height  # Increase the offset

            cursor.close()
            connection.close()

        except psycopg2.Error as error:
            print("Error: Failed to fetch high scores "
                  "from the database:", error)

    def draw_screen(self):
        """Draw the end screen elements to the screen."""
        # Draw the background image first.
        self.screen.blit(self.background_image, (0, 0))

        self.screen.blit(self.title_image, self.title_rect)
        self.play_button.draw_button()
        self.screen.blit(self.high_scores_image, self.high_scores_image_rect)

        # Blit each score string onto the main screen
        for score_image, score_rect in self.score_strings:
            self.screen.blit(score_image, score_rect)

        pygame.display.flip()

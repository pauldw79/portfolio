"""Module for tracking game statistics"""


class GameStats:
    """Track statistics fro Alien Invasion."""

    def __init__(self, ai_game):
        """Initialize statistics."""
        self.settings = ai_game.settings
        self.reset_stats()

        self.game_active = False

        self.high_score = 0
        self.game_started = False
        self.high_score_saved = False

    def reset_stats(self):
        """Initialize statistics that can change during the game."""
        self.ships_left = self.settings.ship_limit
        self.score = 0
        self.level = 1

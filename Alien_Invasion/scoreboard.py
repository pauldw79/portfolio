"""
Scoreboard Module
This module contains the Scoreboard class responsible
for displaying and managing scoring information.
"""


import pygame.font
import psycopg2
from pygame.sprite import Group
from ship import Ship
from db import db_config


class Scoreboard:
    """A class to report scoring information."""

    def __init__(self, ai_game):
        """Initialize scorekeeping attributes."""
        self.ai_game = ai_game
        self.screen = ai_game.screen
        self.screen_rect = self.screen.get_rect()
        self.settings = ai_game.settings
        self.stats = ai_game.stats
        self.db_config = db_config
        self.best_score_image = None
        self.best_score_rect = None

        # Font setting for scoring information.
        self.text_color = pygame.Color(255, 255, 255)
        self.font = pygame.font.SysFont(None, 48)

        # Prepare the initial score images.
        self.prep_score()
        self.update_best_score()
        self.prep_level()
        self.prep_ships()

    def prep_score(self):
        """Turn the score into a rendered image."""
        rounded_score = round(self.stats.score, -1)
        score_str = f"Score: {rounded_score:,}"
        self.score_image = self.font.render(
            score_str, True, self.text_color, None)

        # Position the score at the top left of the screen with some spacing.
        self.score_rect = self.score_image.get_rect()
        self.score_rect.left = self.screen_rect.left + 20
        self.score_rect.top = 20

    def prep_level(self):
        """Turn the level into a rendered image."""
        level_str = f"Level: {self.stats.level}"
        self.level_image = self.font.render(
            level_str, True, self.text_color, None)
        self.level_rect = self.level_image.get_rect()
        self.level_rect.right = self.screen_rect.right - 20
        self.level_rect.top = 20

    def prep_ships(self):
        """Show how many ships are left."""
        self.ships = Group()
        ship_spacing = 10
        ship_width = 32
        total_ships_width = (
            ship_width + ship_spacing) * self.stats.ships_left - ship_spacing
        x_position = self.screen_rect.right - total_ships_width - 10

        for _ in range(self.stats.ships_left):
            ship = Ship(self.ai_game)
            ship.rect.x = x_position
            ship.rect.y = self.screen_rect.bottom - 10 - ship.rect.height
            ship.rect.width = ship_width  # Set ship width
            ship.image = pygame.transform.scale(
                ship.image, (ship_width, ship.rect.height))
            self.ships.add(ship)
            x_position += ship_width + ship_spacing

    def update_best_score(self):
        """Retrieve and update the best score from the database."""
        try:
            connection = psycopg2.connect(**self.db_config)
            cursor = connection.cursor()

            select_query = (
                "SELECT MAX(score) "
                "FROM high_scores;"
            )
            cursor.execute(select_query)
            best_score = cursor.fetchone()

            # Create a rendered image for the best score
            if best_score[0] is not None:
                best_score_str = f"High Score: {best_score[0]:,}"
            else:
                best_score_str = "High Score: 0"

            self.best_score_image = self.font.render(
                best_score_str, True, self.text_color, None)

            self.best_score_rect = self.best_score_image.get_rect()
            self.best_score_rect.centerx = self.screen_rect.centerx
            self.best_score_rect.top = 20

            cursor.close()
            connection.close()

        except psycopg2.Error as error:
            print(
                "Error: Failed to fetch best score from the database:", error
            )

    def show_score(self):
        """Draw the scores, level, and ships to the screen."""
        self.screen.blit(self.score_image, self.score_rect)
        self.screen.blit(self.level_image, self.level_rect)
        self.screen.blit(self.best_score_image, self.best_score_rect)
        self.ships.draw(self.screen)

    def save_score_to_database(self):
        """
        Save the high score to the database if it's in the top 5,
        and the game is over.
        """
        if not self.stats.game_active:
            try:
                connection = psycopg2.connect(**self.db_config)
                cursor = connection.cursor()

                # Fetch the top 5 scores from the database
                select_query = (
                    "SELECT score "
                    "FROM high_scores "
                    "ORDER BY score DESC "
                    "LIMIT 5;"
                )
                cursor.execute(select_query)
                top_scores = [score[0] for score in cursor.fetchall()]

                # Check if the current score is in the top 5
                if self.stats.score > min(top_scores) or len(top_scores) < 5:
                    # Insert the high score into the database
                    insert_query = (
                        "INSERT INTO high_scores (score) "
                        "VALUES (%s);"
                    )
                    cursor.execute(insert_query, (self.stats.score,))
                    print("High score saved to the database.")

                    self.stats.high_score_saved = True

                cursor.close()
                connection.close()

            except psycopg2.Error as error:
                print("Error: Failed to fetch high scores "
                      "from the database:", error)

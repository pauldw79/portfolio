"""Module for managing game settings."""


import random
import pygame


class Settings:
    """Class to manage the game's settings."""

    def __init__(self):
        """Initialize the game's static settings."""
        # Screen settings.
        self.screen_width = 1000
        self.screen_height = 750

        # Background color and image settings
        self.bg_color = self._generate_random_color()
        self.city_image = pygame.image.load("Images/city.png")
        self.city_image = pygame.transform.scale(
            self.city_image, (self.screen_width, self.city_image.get_height()))
        self.city_rect = self.city_image.get_rect()
        self.city_rect.bottom = self.screen_height + 175

        # Ship settings
        self.ship_limit = 3

        # Bullet settings
        self.bullet_width = 5
        self.bullet_height = 15
        self.bullet_color = (139, 0, 0)
        self.bullets_allowed = 5

        # Alien settings
        self.fleet_drop_speed = 5

        # Scoring (Note: Alien points are dynamically set later)
        self.alien_points = None

        # How quickly the game speeds up.
        self.speedup_scale = 1.1

        # How quickly the alien point values increase.
        self.score_scale = 1.5

        self.initialize_dynamic_settings()

    def _generate_random_color(self):
        return pygame.Color(
            random.randint(0, 255),
            random.randint(0, 255),
            random.randint(0, 255)
        )

    def initialize_dynamic_settings(self):
        """Initialize settings that change throughout the game."""
        self.ship_speed = 1.5
        self.bullet_speed = 3.0
        self.alien_speed = 1.0

        # fleet_direction of 1 represents right; -1 represents left.
        self.fleet_direction = 1

        # Scoring
        self.alien_points = 50

    def increase_speed(self):
        """Increase speed settings and alien point values."""
        self.ship_speed *= self.speedup_scale
        self.bullet_speed *= self.speedup_scale
        self.alien_speed *= self.speedup_scale

        self.alien_points = int(self.alien_points * self.score_scale)

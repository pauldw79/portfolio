"""
Ship Module
This module contains the Ship class responsible for managing the player's ship.
"""

import pygame
from pygame.sprite import Sprite


class Ship(Sprite):
    """
    A class to manage the ship.

    Attributes:
        screen (pygame.Surface): The game screen.
        settings (Settings): The game settings.
        screen_rect (pygame.Rect): The screen's rect.
        image (pygame.Surface): The ship's image.
        rect (pygame.Rect): The ship's rect.
        x (float): The horizontal position of the ship.
        moving_right (bool): Flag to indicate if the ship is moving right.
        moving_left (bool): Flag to indicate if the ship is moving left.
    """

    def __init__(self, ai_game):
        """
        Initializes the ship and sets its starting position.

        Args:
            ai_game (AlienInvasion): The main game instance.
        """
        super().__init__()
        self.screen = ai_game.screen
        self.settings = ai_game.settings
        self.screen_rect = ai_game.screen.get_rect()

        # Load the ship image and get its rect.
        self.image = pygame.image.load('Images/ship.bmp')
        self.image = pygame.transform.scale(self.image, (50, 50))
        self.rect = self.image.get_rect()

        # Start each new ship at the bottom center of the screen.
        self.rect.midbottom = self.screen_rect.midbottom

        # Store a decimal value for the ship's horizontal position.
        self.x = float(self.rect.x)

        # Movement flags
        self.moving_right = False
        self.moving_left = False

    def update(self):
        """
        Update the ship's position based on the movement flags.
        """
        # Update the ship's x value, not the rect.
        if self.moving_right and self.rect.right < self.screen_rect.right:
            self.x += self.settings.ship_speed
        if self.moving_left and self.rect.left > 0:
            self.x -= self.settings.ship_speed

        # Update the rect object from self.x.
        self.rect.x = self.x

    def blitme(self):
        """
        Draw the ship at its current location.
        """
        self.screen.blit(self.image, self.rect)

    def center_ship(self):
        """
        Center the ship on the screen.
        """
        self.rect.midbottom = self.screen_rect.midbottom
        self.x = float(self.rect.x)

"""
Sounds Module
This module defines the Sounds class for managing
game sounds in Alien Invasion.
"""


import pygame


class Sounds:
    """
    A class to manage game sounds.

    Attributes:
        music (pygame.mixer.Sound): The background music for the game.
        original_length (float): The original length of the
        background music in seconds.
    """

    def __init__(self):
        """
        Initialize the Sounds class and load the game sounds.
        """
        pygame.mixer.init()
        self.music = pygame.mixer.Sound('Sounds/space_music.wav')
        self.original_length = self.music.get_length()

    def play(self):
        """
        Play the background music continuously.

        This method loops the background music to play it continuously.
        """
        self.music.play(loops=-1)

    def stop(self):
        """
        Stop playing the background music.

        This method stops the background music playback.
        """
        self.music.stop()

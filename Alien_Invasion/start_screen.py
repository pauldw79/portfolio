"""Module for defining the start screen of the game."""

import pygame.font

from pygame.sprite import Sprite


class StartButton(Sprite):
    """A class to represent a button on the start screen."""

    def __init__(self, ai_game, msg):
        """Initialize button attributes.

        Args:
            ai_game (object): The main game object.
            msg (str): The message displayed on the button.
        """
        super().__init__()
        self.screen = ai_game.screen
        self.screen_rect = self.screen.get_rect()

        # Set the dimensions and properties of the button.
        self.width, self.height = 200, 50
        self.button_color = (0, 255, 0)
        self.text_color = (255, 255, 255)
        self.font = pygame.font.SysFont(None, 48)

        # Build the button's rect object and center it.
        self.rect = pygame.Rect(0, 0, self.width, self.height)
        self.rect.center = self.screen_rect.center
        self.rect.bottom = self.screen_rect.bottom - 150

        # The button message needs to be prepped only once.
        self._prep_msg(msg)

        # Initialize clicked attribute
        self.clicked = False

    def _prep_msg(self, msg):
        """Turn msg into a rendered image and center text on the button.

        Args:
            msg (str): The message to display on the button.
        """
        self.msg_image = self.font.render(
            msg, True, self.text_color, self.button_color)
        self.msg_image_rect = self.msg_image.get_rect()
        self.msg_image_rect.center = self.rect.center

    def draw_button(self):
        """Draw the button to the screen."""
        self.screen.fill(self.button_color, self.rect)
        self.screen.blit(self.msg_image, self.msg_image_rect)


class StartScreen:
    """A class to represent the end screen of the game."""

    def __init__(self, ai_game):
        """Initialize end screen attributes.

        Args:
            ai_game (object): The main game object.
            db_config (dict): Database configuration parameters.
        """
        self.screen = ai_game.screen
        self.screen_rect = self.screen.get_rect()
        self.settings = ai_game.settings

        # Load the background image and scale it to fit the screen.
        self.background_image = pygame.image.load('Images/ss_bg.jpg').convert()
        self.background_image = pygame.transform.scale(
            self.background_image, self.screen_rect.size)

        self.title = "Alien Invasion"
        self.font = pygame.font.SysFont(None, 100)
        self.title_image = pygame.Surface((500, 200), pygame.SRCALPHA)
        self.title_image.fill((255, 255, 255, 0))
        text = self.font.render(self.title, True, (255, 255, 255))
        self.title_image.blit(text, (0, 0))
        self.title_rect = self.title_image.get_rect()
        self.title_rect.centerx = self.screen_rect.centerx
        self.title_rect.y = self.screen_rect.top + 50

        self.play_button = StartButton(ai_game, "Play")

        instructions_text = "Use arrow keys to move, Spacebar to shoot"
        self.instructions_font = pygame.font.SysFont(None, 36)
        self.instructions_image = self.instructions_font.render(
            instructions_text, True, (255, 255, 255))
        self.instructions_image_rect = self.instructions_image.get_rect(
            bottomright=(
                self.screen_rect.right - 10,
                self.screen_rect.bottom - 10
            )
        )

    def draw_screen(self):
        """Draw the start screen elements to the screen."""
        self.screen.blit(self.background_image, (0, 0))

        self.screen.blit(self.title_image, self.title_rect)
        self.play_button.draw_button()
        self.screen.blit(self.instructions_image, self.instructions_image_rect)
        pygame.display.flip()

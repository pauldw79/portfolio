"""Set the rules for a minimum bet."""
class MinBet:
    def __init__(self):
        self.min_bet_amount = 100

    def get_bet_amount(self):
        while True:
            bet_amount = input("Please enter your bet amount: ")
            print()
            try:
                bet_amount = int(bet_amount)
                if bet_amount < self.min_bet_amount:
                    print(
                        "Error: Your bet must be at least\n",
                        self.min_bet_amount
                    )
                else:
                    return bet_amount
            except ValueError:
                print("Error: Invalid bet amount. Please enter a number.")

# Track the player's money                
class PlayerMoney:
    
    def __init__(self):
        self.initial_amount = 10000
        self.current_amount = self.initial_amount
        
    def track_player_money(self, bet_amount, win):
        if win:
            self.current_amount += bet_amount
        else:
            self.current_amount -= bet_amount
        if self.current_amount <= 0:
            return False
        else:
            return True

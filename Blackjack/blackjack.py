import random
import colorama
from colorama import Fore
import time

 
 
class Card:
 
    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank
    def __str__(self):   
        return f"{self.rank['rank']} of {self.suit}"
 
class Deck:
 
 # Clubs is \u2663, Hearts is \u2665, Spades is \u2660, Diamonds is \u2666  
    def __init__(self):
        self.cards = []
        suits = [Fore.BLACK + "\u2663" + Fore.RESET, Fore.RED + "\u2665" + Fore.RESET, 
                 Fore.BLACK + "\u2660" + Fore.RESET, Fore.RED + "\u2666" + Fore.RESET]
        ranks = [
                {"rank": "Ace", "value": 11},
                {"rank": 2, "value": 2},
                {"rank": 3, "value": 3},
                {"rank": 4, "value": 4},
                {"rank": 5, "value": 5},
                {"rank": 6, "value": 6},
                {"rank": 7, "value": 7},
                {"rank": 8, "value": 8},
                {"rank": 9, "value": 9},
                {"rank": 10, "value": 10},
                {"rank": "Jack", "value": 10},
                {"rank": "Queen", "value": 10},
                {"rank": "King", "value": 10},
            ]
 
        for suit in suits:
            for rank in ranks:
                self.cards.append(Card(suit, rank))     
 
    def shuffle(self):
        if len(self.cards) > 1:
              random.shuffle(self.cards)
 
    def deal(self,number):
        cards_dealt = []
        for i in range(number):
            if len(self.cards) > 0:
                card = self.cards.pop()
                cards_dealt.append(card)
        return cards_dealt
 
class Hand:
 
    def __init__(self, dealer = False):
        self.cards = []
        self.value = 0
        self.dealer = dealer
 
    def add_card(self, card_list):
        self.cards.extend(card_list)
 
    def calculate_value(self):
        self.value = 0
        has_ace = False
 
 
        for card in self.cards:
            card_value = int(card.rank["value"])
            self.value += card_value
            if card.rank["rank"] == "Ace":
                has_ace = True
 
        if has_ace and self.value > 21:
            self.value -= 10
 
    def get_value(self):
        self.calculate_value()
        return self.value
 
    def is_blackjack(self):
        return self.get_value() == 21
 
    def display(self, show_all_dealer_cards = False):
 
        print(f'''{"Dealer's" if self.dealer else "Your"} hand:''')
        for index, card in enumerate(self.cards):
            if index == 0 and self.dealer and not show_all_dealer_cards \
            and not self.is_blackjack():
 
                print("HIDDEN")
            else:
                print(card)
 
        if not self.dealer:
            print("Value:", self.get_value())
            print()
 
 
class Game:
 
    print("Black Jack game by Paul Williams\n")
 
    def __init__(self):
        self.player_wins = 0
 
    def play(self):
        game_number = 0
        games_to_play = 0
 
        while games_to_play <=0:
            try:
                games_to_play = int(input("How many games would you like to play? "))
            except:
                print("You must enter a number.")
 
        while game_number < games_to_play:
            game_number += 1
            deck = Deck()
            deck.shuffle()
 
            player_hand = Hand()
            dealer_hand = Hand(dealer = True)
 
            for i in range(2):
                player_hand.add_card(deck.deal(1))
                dealer_hand.add_card(deck.deal(1))
 
            print()
            print(*"*" * 8)
            print(f"Game {game_number} of {games_to_play}")
            print(*"*" * 8)
            player_hand.display()
            time.sleep(1)
            dealer_hand.display()
            time.sleep(1)
 
            if self.check_winner(player_hand, dealer_hand):
                continue
 
            choice = ""
            while player_hand.get_value() < 21 and choice not in ["s", "stay"]:
                choice = input("Would you like to 'Hit' or 'Stay'? ").lower()
                print()
                while choice not in ["h", "hit", "s", "stay"]:
                    choice = input("Please input 'Hit' or 'Stay' (or h/s)").lower()
                    print()
                if choice in ["h", "hit"]:
                    player_hand.add_card(deck.deal(1))
                    player_hand.display()
                    time.sleep(1)
 
            if self.check_winner(player_hand, dealer_hand):
                continue
 
            player_hand_value = player_hand.get_value()
            dealer_hand_value = dealer_hand.get_value()
 
            while dealer_hand_value < 17:
                dealer_hand.add_card(deck.deal(1))
                dealer_hand_value = dealer_hand.get_value()
 
            dealer_hand.display(show_all_dealer_cards = True)
 
            if self.check_winner(player_hand, dealer_hand):
                continue
 
            print("Final Results")
            print("Your hand:", player_hand_value)
            print("Dealer's hand:", dealer_hand_value)
 
            self.check_winner(player_hand, dealer_hand, True)
 
 
        print("\nThank you for playing!")
        print(f"You won {self.player_wins} out of {games_to_play} games.")
 
    def check_winner(self, player_hand, dealer_hand, game_over = False):
        if not game_over:
            if player_hand.get_value() > 21:
                print("You busted. Dealer wins!")
                time.sleep(1)
                return True
            elif dealer_hand.get_value() > 21:
                print("Dealer busted. You win!")
                time.sleep(1)
                self.player_wins += 1
                return True
            elif dealer_hand.is_blackjack() and player_hand.is_blackjack():
                print("Both players have Blackjack! It's a tie!")
                time.sleep(1)
                return True
            elif player_hand.is_blackjack():
                print("You have a Blackjack! You win!")
                time.sleep(1)
                self.player_wins += 1
                return True
            elif dealer_hand.is_blackjack():
                print("Dealer has a Blackjack! Dealer wins!")
                time.sleep(1)
                return True
        else:
            if player_hand.get_value() > dealer_hand.get_value():
                print("You win!")
                time.sleep(1)
                self.player_wins += 1
            elif player_hand.get_value() == dealer_hand.get_value():
                print("It's a tie!")
                time.sleep(1)
            else:
                print("Dealer wins!")
                time.sleep(1)
            return True   
 
        return False
 
g = Game()
g.play()
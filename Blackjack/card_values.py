import os
from collections import Counter
from card_deck import Card, Deck

class PokerHandEvaluator:
    def __init__(self):
        self.deck = Deck()

    def card_sort_key(self, card):
        return (card.rank["value"] if isinstance(card.rank, dict) else card.rank, card.suit)

    def rank_poker_hand(self, hand):
        ranks = [card.rank["rank"] if isinstance(card.rank, dict) else card.rank for card in hand]
        suits = [card.suit for card in hand]
        values_count = Counter(ranks)

        flush = len(set(suits)) == 1

        values = sorted([card.rank["value"] if isinstance(card.rank, dict) else card.rank for card in hand])
        if values == [2, 3, 4, 5, 14]:
            straight = True
        else:
            straight = (max(values) - min(values) == 4) and len(set(values)) == 5

        straight_flush = straight and flush
        royal_flush = straight_flush and max(values) == 14

        four_of_a_kind = any(count == 4 for count in values_count.values())
        full_house = (any(count == 3 for count in values_count.values()) and any(count == 2 for count in values_count.values()))
        three_of_a_kind = any(count == 3 for count in values_count.values())

        pairs = [rank for rank, count in values_count.items() if count == 2]
        two_pair = len(pairs) == 2

        one_pair = any(count == 2 for count in values_count.values())

        if royal_flush:
            return "Royal Flush"
        elif straight_flush:
            return "Straight Flush"
        elif four_of_a_kind:
            return "Four of a Kind"
        elif full_house:
            return "Full House"
        elif flush:
            return "Flush"
        elif straight:
            return "Straight"
        elif three_of_a_kind:
            return "Three of a Kind"
        elif two_pair:
            return "Two Pair"
        elif one_pair:
            return "One Pair"
        else:
            return "High Card"

    def hand_total_value(self, hand):
        total = sum([card.rank["value"] for card in hand])
        return total

    def evaluate_hands(self, num_hands=10):
        os.system("clear")
        for i in range(num_hands):
            self.deck.shuffle()
            hand = self.deck.deal_card(5)
            sorted_hand = sorted(hand, key=self.card_sort_key)
            for card in sorted_hand:
                print(card.rank['rank'], "of", card.suit)
            print(self.rank_poker_hand(hand))
            print(f"Total value: {self.hand_total_value(hand)}")
            print()
from card_deck import Cards, Deck


# Creating player's 2-card hand.
class PlayerHand:
    def __init__(self):
        self.hand = []
        self.deck = Deck()
        self.deck.shuffle()

    def draw_initial_hand(self):
        self.hand = self.deck.deal_card()
        self.cards = self.deck.deal_card(2)
        print("Player Hand:")
        for card in self.cards:
               print(card)


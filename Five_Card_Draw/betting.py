"""
Module: betting

This module provides classes for managing bets and player money in a card game.
"""

class MinBet:
    """
    Set the rules for a minimum bet.
    """

    def __init__(self):
        self.min_bet_amount = 100

    def get_bet_amount(self):
        """
        Get the player's bet amount.

        Returns:
            int: The player's bet amount.
        """
        while True:
            bet_amount = input("Please enter your bet amount: ")
            print()
            try:
                bet_amount = int(bet_amount)
                if bet_amount < self.min_bet_amount:
                    print("Error: Your bet must be at least", self.min_bet_amount)
                else:
                    return bet_amount
            except ValueError:
                print("Error: Invalid bet amount. Please enter a number.")


class PlayerMoney:
    """
    Track the player's money.
    """

    def __init__(self, initial_amount=10000):
        self.initial_amount = initial_amount
        self.current_amount = self.initial_amount

    def set_initial_amount(self, amount):
        """
        Set the initial amount of money.
        """
        self.initial_amount = amount
        self.current_amount = amount

    def track_player_money(self, bet_amount, win):
        """
        Update the player's money based on the bet result.
        """
        if win:
            self.current_amount += bet_amount
        else:
            self.current_amount -= bet_amount
        return self.current_amount > 0

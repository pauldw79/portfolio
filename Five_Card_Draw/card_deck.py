"""
Module: card_deck
This module defines classes related to a standard deck of playing cards.
"""

import random
from colorama import Fore

class Values:
    """
    Class to define card values and suits.
    """
    suits = [
        Fore.BLACK + "\u2663" + Fore.RESET,
        Fore.RED + "\u2665" + Fore.RESET,
        Fore.BLACK + "\u2660" + Fore.RESET,
        Fore.RED + "\u2666" + Fore.RESET
    ]

    ranks = [
        {"rank": "Ace", "value": 14},
        {"rank": 2, "value": 2},
        {"rank": 3, "value": 3},
        {"rank": 4, "value": 4},
        {"rank": 5, "value": 5},
        {"rank": 6, "value": 6},
        {"rank": 7, "value": 7},
        {"rank": 8, "value": 8},
        {"rank": 9, "value": 9},
        {"rank": 10, "value": 10},
        {"rank": "Jack", "value": 11},
        {"rank": "Queen", "value": 12},
        {"rank": "King", "value": 13},
    ]

class Card:
    """
    Class to represent a single playing card.
    """

    def __init__(self, suit, rank):
        self.suit = suit
        self.rank = rank

    def __str__(self):
        """
        Return a string representation of the card.
        """
        if isinstance(self.rank, dict):
            return f"{self.rank['rank']} of {self.suit} ({self.rank['value']})"

        return f"{self.rank} of {self.suit}"

class Deck:
    """
    Class to represent a deck of playing cards.
    """

    def __init__(self):
        self.cards = []
        for suit in Values.suits:
            for rank in Values.ranks:
                self.cards.append(Card(suit, rank))
        self.dealt_cards = []

    def shuffle(self):
        """
        Shuffle the deck of cards.
        """
        random.shuffle(self.cards)

    def deal_card(self, num_cards=1):
        """
        Deal one or more cards from the deck.

        Args:
            num_cards (int): The number of cards to deal.

        Returns:
            list: A list of dealt cards.
        """
        dealt_cards = []
        for _ in range(num_cards):
            if len(self.cards) == 0:
                print("Error: There are no more cards in the deck.")
                break
            card = self.cards.pop()
            dealt_cards.append(card)
            self.dealt_cards.append(card)
        return dealt_cards

    def __str__(self):
        """
        Return a string representation of the deck.
        """
        deck_str = ""
        for card in self.cards:
            deck_str += str(card) + "\n"
        return deck_str

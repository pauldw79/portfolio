import pygame
import random

# Initialize Pygame
pygame.init()

# Set up the screen
screen = pygame.display.set_mode((800, 600))
pygame.display.set_caption("Card Drawing")

# Load card images (replace with your image paths)
card_width, card_height = 100, 150  # Set the desired width and height
card_gap = 5  # Set the gap between cards

cards = [
    pygame.transform.scale(pygame.image.load("PNG-cards-1.3/ace_of_spades.png"), (card_width, card_height)),
    pygame.transform.scale(pygame.image.load("PNG-cards-1.3/ace_of_hearts.png"), (card_width, card_height)),
    pygame.transform.scale(pygame.image.load("PNG-cards-1.3/ace_of_clubs.png"), (card_width, card_height)),
    pygame.transform.scale(pygame.image.load("PNG-cards-1.3/ace_of_diamonds.png"), (card_width, card_height)),
    pygame.transform.scale(pygame.image.load("PNG-cards-1.3/king_of_spades.png"), (card_width, card_height)),
]

# Shuffle the cards
random.shuffle(cards)

# Main game loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    # Draw cards centered on the screen
    screen.fill((100, 100, 100))  # Clear the screen

    total_width = len(cards) * (card_width + card_gap) - card_gap  # Account for the gap
    x = (800 - total_width) // 2
    y = (600 - card_height) // 2

    for card in cards:
        screen.blit(card, (x, y))
        x += card_width + card_gap  # Update x position with gap

    pygame.display.update()

# Quit Pygame
pygame.quit()

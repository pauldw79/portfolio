"""
Module: card_values
This module defines classes related to valuing a 5 card hand.
"""

import os
from colorama import Fore
from collections import Counter
from card_deck import Deck

class Values:
    # Setting the suit and value of all 52 cards.
    suits = [Fore.BLACK + "\u2663" + Fore.RESET, Fore.RED + "\u2665" + Fore.RESET,
            Fore.BLACK + "\u2660" + Fore.RESET, Fore.RED + "\u2666" + Fore.RESET]

    ranks = [
        {"rank": "Ace", "value": 14},
        {"rank": 2, "value": 2},
        {"rank": 3, "value": 3},
        {"rank": 4, "value": 4},
        {"rank": 5, "value": 5},
        {"rank": 6, "value": 6},
        {"rank": 7, "value": 7},
        {"rank": 8, "value": 8},
        {"rank": 9, "value": 9},
        {"rank": 10, "value": 10},
        {"rank": "Jack", "value": 11},
        {"rank": "Queen", "value": 12},
        {"rank": "King", "value": 13},
    ]

class PokerHandEvaluator:
    """Class for evaluating poker hands."""

    def __init__(self):
        """Initialize the PokerHandEvaluator with a deck."""
        self.deck = Deck()
        self.values = Values()

    def card_sort_key(self, card):
        """
        Define a sorting key for cards.
        """
        return (
            card.rank["value"] if isinstance(card.rank, dict) else card.rank,
            card.suit,
        )

    def rank_poker_hand(self, hand):
        """
        Rank a poker hand.
        """
        ranks = [card.rank["rank"] if isinstance(
            card.rank, dict) else card.rank for card in hand]
        suits = [card.suit for card in hand]
        values_count = Counter(ranks)
        
        # Extract card values from the Values class
        card_values = [card["value"] for card in self.values.ranks]

        total_values = [card_values.index(card.rank["value"] if isinstance(card.rank, dict) else card.rank) for card in hand]

        flush = len(set(suits)) == 1

        values = sorted([card.rank["value"] if isinstance(
            card.rank, dict) else card.rank for card in hand])

        straight = (max(values) - min(values) == 4) and len(set(values)) == 5

        straight_flush = straight and flush
        royal_flush = straight_flush and max(values) == 14

        four_of_a_kind = any(count == 4 for count in values_count.values())
        full_house = (
            any(count == 3 for count in values_count.values()) and any(
                count == 2 for count in values_count.values()
            )
        )
        three_of_a_kind = any(count == 3 for count in values_count.values())

        pairs = [rank for rank, count in values_count.items() if count == 2]
        two_pair = len(pairs) == 2

        one_pair = any(count == 2 for count in values_count.values())

        if royal_flush:
            return "Royal Flush"
        if straight_flush:
            return "Straight Flush"
        if four_of_a_kind:
            return "Four of a Kind"
        if full_house:
            return "Full House"
        if flush:
            return "Flush"
        if straight:
            return "Straight"
        if three_of_a_kind:
            return "Three of a Kind"
        if two_pair:
            return "Two Pair"
        if one_pair:
            return "One Pair"

        return "High Card"

    def hand_total_value(self, hand):
        """
        Calculate the total value of a poker hand.
        """
        total = sum(
            card.rank["value"] if isinstance(card.rank, dict) else card.rank for card in hand
        )
        return total

    def evaluate_hands(self, num_hands=10):
        """Evaluate a series of poker hands."""
        os.system("clear")
        for _ in range(num_hands):
            self.deck.shuffle()
            hand = self.deck.deal_card(5)
            sorted_hand = sorted(hand, key=self.card_sort_key)
            for card in sorted_hand:
                print(card.rank['rank'], "of", card.suit)
            print(self.rank_poker_hand(hand))
            print(f"Total value: {self.hand_total_value(hand)}")
            print()

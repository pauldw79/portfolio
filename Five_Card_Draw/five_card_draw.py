"""
Simple 5 card draw game with rudimentary computer AI logic.
"""
import psycopg2
import os
from collections import Counter
from card_deck import Deck
from betting import MinBet, PlayerMoney
from card_values import PokerHandEvaluator
from db import db_config


class FiveCardDrawGame:
    """
    A class representing a simple 5 card draw game
    with rudimentary computer AI logic.
    """
    def __init__(self):
        self.deck = Deck()
        self.min_bet = MinBet()
        self.player_money = None
        self.player_name = None
        self.poker_evaluator = PokerHandEvaluator()

    def clear_screen(self):
        """
        Clear the screen.
        """
        os.system("clear")

    def welcome_message(self):
        """
        Display a welcome message to the player.
        """
        self.clear_screen()
        print("Welcome to Five Card Draw Poker")
        print("=================================")
        print("A simple 5 card draw game with rudimentary computer AI logic.")
        print("Try to beat the computer and win some money!")
        print()

    def get_player_name(self):
        """
        Prompt the player to enter their username.
        """
        self.player_name = input("Enter your username: ")
        print()
        print(f"Welcome, {self.player_name}!\n")
        
    def get_player_stats(self):
        try:
            conn = psycopg2.connect(**db_config)
            
            with conn:
                with conn.cursor() as cursor:
                    cursor.execute(
                        """
                        SELECT balance, wins, losses
                        FROM stats
                        WHERE username = %s
                        """,
                        (self.player_name,)
                    )
                    player_stats = cursor.fetchone()
                    if player_stats:
                        return player_stats
                    else:
                        return (10000, 0, 0)
        except psycopg2.Error as e:
            print(f"Error retrieving player statistics: {e}")
            return None
                    

    def insert_player_stats(self, username, balance, wins, losses):
        try:
            conn = psycopg2.connect(**db_config)
            
            with conn:
                with conn.cursor() as cursor:
                    cursor.execute(
                        """
                        INSERT INTO stats (username, balance, wins, losses)
                        VALUES (%s, %s, %s, %s)
                        ON CONFLICT (username) DO UPDATE
                        SET balance = excluded.balance, wins = excluded.wins, losses = excluded.losses
                        """,
                        (username, balance, wins, losses)
                    )
        except psycopg2.Error as e:
            print(f"Error updating player stats: {e}")

    def display_data(self, username):
        try:
            conn = psycopg2.connect(**db_config)
            
            with conn:
                with conn.cursor() as cursor:
                    cursor.execute(
                        """
                        SELECT username, balance, wins, losses
                        FROM stats
                        WHERE username = %s
                        """,
                        (username,)
                    )
                    player_data = cursor.fetchone()
                    if player_data:
                        username, balance, wins, losses = player_data
                        print(f"Career data for {username}:")
                        print(f"Balance: ${balance}")
                        print(f"Wins: {wins}")
                        print(f"Losses: {losses}")
                    else:
                        print(f"Welcome, {username}! Good luck!")
        except psycopg2.Error as e:
            print(f"Error retrieving career data: {e}")

    def main(self):
        """
        The main game loop.
        """
        self.welcome_message()
        self.get_player_name()
        
        initial_balance, initial_wins, initial_losses = self.get_player_stats()

        self.deck.shuffle()
        self.player_money = PlayerMoney(initial_balance)
        poker_evaluator = PokerHandEvaluator()

        first_game = True
        career_data_updated = False
        wins, losses = initial_wins, initial_losses

        while self.player_money.current_amount > 0:
            
            self.deck = Deck()
            self.deck.shuffle()

            if first_game:
                first_game = False
            else:
                play = input("Do you want to play another round? (yes/no): ")
                print()
                if play.lower() != "yes":
                    break

            balance_message = (
                f"Your current balance, {self.player_name}, "
                f"is ${self.player_money.current_amount}.\n"
            )
            print(balance_message)
            
            if not career_data_updated:
                self.display_data(self.player_name)
                self.insert_player_stats(self.player_name, self.player_money.current_amount, wins, losses)

            bet_amount = self.min_bet.get_bet_amount()

            if bet_amount > self.player_money.current_amount:
                print("Error: Your bet cannot exceed your current balance.\n")
                continue

            player_hand = self.deck.deal_card(5)
            computer_hand = self.deck.deal_card(5)

            print("Your hand:")
            self.display_hand(player_hand)

            player_hand = self.discard_and_draw(player_hand)

            computer_hand = self.computer_discard_and_draw(computer_hand)

            print("\nYour final hand:")
            self.display_hand(player_hand)

            print("\nComputer's final hand:")
            self.display_hand(computer_hand)
            print()

            player_score = poker_evaluator.rank_poker_hand(player_hand)
            computer_score = poker_evaluator.rank_poker_hand(computer_hand)
            player_total_value = poker_evaluator.hand_total_value(player_hand)
            computer_total_value = poker_evaluator.hand_total_value(computer_hand)

            if player_score > computer_score:
                result = f"""{self.player_name}, you win with {player_score}!
                Computer had {computer_score}."""
                self.player_money.track_player_money(bet_amount, win=True)
                wins += 1
                self.insert_player_stats(self.player_name, self.player_money.current_amount, wins, losses)
            elif player_score < computer_score:
                result = f"""Computer wins with {computer_score}!
                You had {player_score}."""
                self.player_money.track_player_money(bet_amount, win=False)
                losses += 1
                self.insert_player_stats(self.player_name, self.player_money.current_amount, wins, losses)
            else:
                if player_total_value > computer_total_value:
                    result = f"""{self.player_name}, you win with {player_score}!
                    Computer had {computer_score}."""
                    self.player_money.track_player_money(bet_amount, win=True)
                    wins += 1
                    self.insert_player_stats(self.player_name, self.player_money.current_amount, wins, losses)
                elif player_total_value < computer_total_value:
                    result = f"""Computer wins with {computer_score}!
                    You had {player_score}."""
                    self.player_money.track_player_money(bet_amount, win=False)
                    losses += 1
                    self.insert_player_stats(self.player_name, self.player_money.current_amount, wins, losses)
                else:
                    result = f"It's a tie! Both have {player_score}!\n"

            print(result)

            career_data_updated = True
            
        if career_data_updated:
            self.display_data(self.player_name)
            self.insert_player_stats(self.player_name, self.player_money.current_amount, wins, losses)

        if self.player_money.current_amount <= 0:
            print(
                f"{self.player_name}, you are all out of money! Game over.\n"
            )
        else:
            final_balance_message = (
                f"Your final balance, {self.player_name}, "
                f"is ${self.player_money.current_amount}.\n"
            )
            print(final_balance_message)

        print("Thanks for playing!")

        self.insert_player_stats(self.player_name, self.player_money.current_amount, wins, losses)


    def display_hand(self, hand):
        """
        Display a player's hand.
        """
        for card in hand:
            print(f"{card.rank['rank']} of {card.suit}")

    def discard_and_draw(self, hand):
        """
        Allow the player to discard and draw card.
        """
        print("Select cards to discard (e.g., 1 3 4): ")
        discard_indices = [int(x) - 1 for x in input().split()]

        for index in discard_indices:
            hand[index] = self.deck.deal_card(1)[0]

        return hand

    def computer_discard_and_draw(self, hand):
        """
        Simulate the computer's discard and draw process.
        """
        values_count = Counter([card.rank["rank"] if isinstance(
            card.rank, dict) else card.rank for card in hand])

        if any(count >= 2 for count in values_count.values()):
            return hand

        for index, card in enumerate(hand):
            if card.rank["rank"] not in ["Jack", "Queen", "King", "Ace"]:
                hand[index] = self.deck.deal_card(1)[0]

        return hand


if __name__ == "__main__":
    game = FiveCardDrawGame()
    game.main()

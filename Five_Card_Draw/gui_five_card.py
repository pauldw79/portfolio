import pygame
import sys
from pygame.locals import QUIT
from card_deck import Values, Deck, CardImages
from inputbox import ask

# Initialize Pygame
pygame.init()

# Set up the window
window_width = 800
window_height = 600
window = pygame.display.set_mode((window_width, window_height))
pygame.display.set_caption("Five Card Draw Poker")

# Create an instance of the CardImages class
card_images = CardImages()
card_images.load_card_images()

# Create an instance of the Deck class
deck = Deck()
deck.shuffle()

# Initialize player and computer hands
player_hand = []
computer_hand = []

# Deal cards back and forth (5 cards each)
for _ in range(5):
    player_card = deck.deal_card(1)[0]
    computer_card = deck.deal_card(1)[0]

    player_hand.append(player_card)
    computer_hand.append(computer_card)

# Print the dealer's hand to the terminal
print("Dealer's Hand: ")
for card in computer_hand:
    print(f"- {card.rank} of {card.suit}")

# Main game loop
running = True
while running:
    for event in pygame.event.get():
        if event.type == QUIT:
            running = False

    # Clear the screen
    window.fill((0, 128, 0))  # Background color

    # Display the text for the dealer's hand
    dealer_font = pygame.font.SysFont(None, 30)
    dealer_text = dealer_font.render("Dealer's Hand", True, (255, 255, 255))
    window.blit(dealer_text, (100, 70))

    # Display the text for the player's hand
    player_font = pygame.font.SysFont(None, 30)
    player_text = player_font.render("Player's Hand", True, (255, 255, 255))
    window.blit(player_text, (100, 270))

    # Display the balance
    balance_font = pygame.font.Font(None, 30)
    balance_text = balance_font.render("Balance: $1000", True, (255, 255, 255))
    balance_position = (window_width - balance_text.get_width() - 20,
                        window_height - balance_text.get_height() - 20)
    window.blit(balance_text, balance_position)

    # Display card images
    x, y = 100, 300
    for card in player_hand:
        suit_str = card.suit.lower()
        rank_str = str(card.rank)
        card_image = card_images.get_card_image(card)
        window.blit(card_image, (x, y))
        x += 20

    # Display the back of the cards for the computer's hand
    x, y = 100, 100
    for _ in computer_hand:
        card_image = card_images.get_card_image("blue_back")
        window.blit(card_image, (x, y))
        x += 20

    # Display text input for username
    username_font = pygame.font.Font(None, 30)
    username_text = username_font.render("Enter Username:", True, (255, 255, 255))
    window.blit(username_text, (window_width - 200, 20))

    # Ask for the username
    x, y = window_width - 200, 60
    username = ask(window, "Enter Username", x, y)
    print(f"Entered Username: {username}")
    
    pygame.display.flip()

# Quit Pygame
pygame.quit()
sys.exit()

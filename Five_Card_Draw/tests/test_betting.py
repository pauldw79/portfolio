import pytest
from unittest.mock import patch
from betting import MinBet, PlayerMoney

def test_min_bet():
    with patch('builtins.input', return_value='100'):
        min_bet = MinBet()
        assert min_bet.get_bet_amount() >= 100
    
def test_player_money():
    initial_amount = 10000
    player_money = PlayerMoney()
    assert player_money.current_amount == initial_amount
    
    player_money.track_player_money(100, win=True)
    assert player_money.current_amount == initial_amount + 100
    
    player_money.track_player_money(50, win=False)
    assert player_money.current_amount == initial_amount + 100 - 50
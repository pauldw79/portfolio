import pytest
from card_deck import Card, Deck

def test_card_creation():
    suit = 'Fore.BLACK + "\u2663" + Fore.RESET'
    rank = "Ace"
    card = Card(suit, rank)
    assert card.suit == suit
    assert card.rank == rank

def test_card_string_representation():
    suit = 'Fore.BLACK + "\u2663" + Fore.RESET'
    rank = "10"
    card = Card(suit, rank)
    assert str(card) == f"{rank} of {suit}"

def test_deck_shuffle():
    deck = Deck()
    initial_order = deck.cards.copy()
    deck.shuffle()
    assert deck.cards != initial_order

def test_deck_deal_card():
    deck = Deck()
    num_cards_to_deal = 5
    dealt_cards = deck.deal_card(num_cards_to_deal)
    assert len(dealt_cards) == num_cards_to_deal
    assert len(deck.cards) == 52 - num_cards_to_deal
    assert all(card not in deck.cards for card in dealt_cards)


if __name__ == '__main__':
    pytest.main()
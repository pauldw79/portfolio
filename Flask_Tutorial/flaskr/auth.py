import functools
from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for, current_app
)
from werkzeug.security import check_password_hash, generate_password_hash

import psycopg2.extras

from flaskr.db import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')

@bp.route('/register', methods=('GET', 'POST'))
def register():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        connection = get_db(current_app.config['DATABASE'])
        cursor = connection.cursor()
        error = None

        if not username:
            error = 'Username is required.'
        elif not password:
            error = 'Password is required.'

        if error is None:
            try:
                cursor.execute(
                    'INSERT INTO "user" (username, password) VALUES (%s, %s)',
                    (username, generate_password_hash(password)),
                )
                connection.commit()
            except psycopg2.IntegrityError:
                error = f"User {username} is already registered."
            else:
                return redirect(url_for("auth.login"))

        flash(error)
        cursor.close()

    return render_template('auth/register.html')

@bp.route('/login', methods=('GET', 'POST'))
def login():
    error = None
    
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        cursor = get_db(current_app.config['DATABASE']).cursor()

        try:
            cursor.execute(
                'SELECT * FROM "user" WHERE username = %s', (username,)
            )
            user = cursor.fetchone()

            if user is None:
                error = 'Incorrect username.'
            elif not check_password_hash(user[2], password):  # Use integer index for password
                error = 'Incorrect password.'
                
            if error is None:
                session.clear()
                session['user_id'] = user[0]  # Use integer index for user id
                return redirect(url_for('index'))

        except Exception as e:
            error = 'An error occurred while processing your request.'

        finally:
            cursor.close()

        flash(error)

    return render_template('auth/login.html')


@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        cursor = get_db(current_app.config['DATABASE']) \
            .cursor(cursor_factory=psycopg2.extras.RealDictCursor)
        cursor.execute(
            'SELECT * FROM "user" WHERE id = %s', (user_id,)
        )
        g.user = cursor.fetchone()
        cursor.close()

        
@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))

def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view

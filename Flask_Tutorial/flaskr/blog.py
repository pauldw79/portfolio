from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, current_app
)
from werkzeug.exceptions import abort

from flaskr.auth import login_required
from flaskr.db import get_db

import psycopg2.extras

bp = Blueprint('blog', __name__)

@bp.route('/')
def index():
    connection = get_db(current_app.config['DATABASE'])
    cursor = connection.cursor()

    query = (
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN "user" u ON p.author_id = u.id'
        ' ORDER BY created DESC'
    )
    
    cursor.execute(query)
    
    columns = [column[0] for column in cursor.description]
    posts = [dict(zip(columns, row)) for row in cursor.fetchall()]

    cursor.close()
    connection.close()
    
    return render_template('blog/index.html', posts=posts)






@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            connection = get_db(current_app.config['DATABASE'])
            cursor = connection.cursor(cursor_factory=psycopg2.extras.RealDictCursor)
            cursor.execute(
                'INSERT INTO post (title, body, author_id)'
                ' VALUES (%s, %s, %s)',
                (title, body, g.user.get('id'))# Use 'id' column name
            )
            connection.commit()
            cursor.close()
            return redirect(url_for('blog.index'))

    return render_template('blog/create.html')


def get_post(id, check_author=True):
    cursor = get_db(current_app.config['DATABASE']).cursor()

    cursor.execute(
        'SELECT p.id, title, body, created, author_id, username'
        ' FROM post p JOIN "user" u ON p.author_id = u.id'
        ' WHERE p.id = %s',
        (id,)
    )
    post = cursor.fetchone()
    cursor.close()

    if post is None:
        abort(404, f"Post id {id} doesn't exist.")

    if check_author and post[4] != g.user['id']:  # Access author_id using index
        abort(403)

    return post


@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        error = None

        if not title:
            error = 'Title is required.'

        if error is not None:
            flash(error)
        else:
            db = get_db(current_app.config['DATABASE'])
            cursor = db.cursor()
            cursor.execute(
                'UPDATE post SET title = %s, body = %s'
                ' WHERE id = %s',
                (title, body, id)
            )
            db.commit()
            cursor.close()
            return redirect(url_for('blog.index'))

    # Manually create a dictionary from the tuple elements
    post_dict = {
        'id': post[0],
        'title': post[1],
        'body': post[2],
        'created': post[3],
        'author_id': post[4],
        'username': post[5]
    }

    return render_template('blog/update.html', post=post_dict)


@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_post(id)
    db = get_db(current_app.config['DATABASE'])
    cursor = db.cursor()
    cursor.execute('DELETE FROM post WHERE id = %s', (id,))
    db.commit()
    cursor.close()
    return redirect(url_for('blog.index'))


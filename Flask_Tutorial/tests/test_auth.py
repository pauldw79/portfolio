import pytest
from flask import g, session
from flaskr.db import get_db
from flaskr import create_app
from flaskr.db import init_db

@pytest.fixture
def app():
    app = create_app({
        'TESTING': True,
        'DATABASE':{
            'dbname': 'flask-tutorial',
            'user': 'postgres',         
            'password': 'postgres',
            'host': 'localhost',                
            'port': '5432'
        },
    })

    with app.app_context():
        init_db()
        
    yield app
    
    with app.app_context():
        db = get_db()
        db.close()

def test_register(client, app):
    assert client.get('/auth/register').status_code == 200
    response = client.post(
        '/auth/register', data={'username': 'a', 'password': 'a'}
    )
    assert response.headers["Location"] == "/auth/login"

    with app.app_context():
        db_connection = get_db()
        cursor = db_connection.cursor()
        cursor.execute(
            """SELECT * FROM "user" WHERE username = 'a'""",
        )
        assert cursor.fetchone() is not None
        cursor.close()


@pytest.mark.parametrize(('username', 'password', 'message'), (
    ('', '', b'Username is required.'),
    ('a', '', b'Password is required.'),
    ('test', 'test', b'You should be redirected automatically to the target URL:'),
))
def test_register_validate_input(client, username, password, message):
    response = client.post(
        '/auth/register',
        data={'username': username, 'password': password}
    )

    assert message in response.data


def test_login(client, auth):
    pass

@pytest.mark.parametrize(('username', 'password', 'message'), (
    ('a', 'test', b'Incorrect username.'),
    ('test', 'a', b'Incorrect username.'),
))
def test_login_validate_input(auth, username, password, message):
    response = auth.login(password, username)
    
    assert message in response.data

    
def test_logout(client, auth):
    auth.login()

    with client:
        auth.logout()
        assert 'user_id' not in session
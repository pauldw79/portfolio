These files are a compilation of programs I created since I started teaching myself Python in October 2022.

Alien_Invasion is a Pygame app I created after reading Python Crash Course. I added many things to the tutorial. Things like sounds, music, color changing background, start and end screens, and a database with a high score tracker.

Blackjack was the first game I made after doing several beginner level programs.

I made Five_Card_Draw using things I learned and Classes I made while making Blackjack, without a tutorial. There
are definitely some WIP files. But a rudimentary text base game is possible to play.

Flask-Tutorial was made using an online guide to learn how to use flask. It's a shared blogging site. It used SQLite for the tutorial, but then I converted it to PostgreSQL. I learned how to "Dockerize" this program.

I threw in some small projects I learned along the way.

1/22/24 I created a weather API checker that will email the current weather and a 3 day forecast. Called API_Email

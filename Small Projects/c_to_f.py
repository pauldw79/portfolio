def c_to_f():
    print("Celsius to Fahrenheit")
    celsius = float(input("Enter temp in Celsius: "))
    fahrenheit = (celsius * 1.8000) + 32
    print(f"""Converting {celsius}C to Fahrenheit equals 
          {fahrenheit}F.""")
    
c_to_f()
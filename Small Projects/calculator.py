import os

abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

print("Simple Calculator")

def calculator():
    num_1 = int(input("Enter the first number: "))
    num_2 = int(input("Enter the second number: "))
    operand = input("Add, subtract, multiply, or divide? ")
    
    if operand.lower() == "add" or operand == "+":
        result = num_1 + num_2
        print(f"Adding {num_1} to {num_2} equals {result}.")
    elif operand.lower() == "subtract" or operand == "-":
        result = num_1 - num_2
        print(f"Subtracting {num_2} from {num_1} equals {result}.")
    elif operand.lower() == "multiply" or operand == "*":
        result = num_1 * num_2
        print(f"Multiplying {num_1} by {num_2} equals {result}.")
    elif operand.lower() == "divide"  or operand == "/":
        result = num_1 / num_2
        print(f"Dividing {num_1} by {num_2} equals {result}.")
        
calculator()
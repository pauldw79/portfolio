def count_vowels():
    statement = input("Enter anything and I'll tell you how many vowels: ")
    count_a = 0
    count_e = 0
    count_i = 0
    count_o = 0
    count_u = 0
    for i in statement:
        if i.lower() == "a":
            count_a += 1
        elif i.lower() == "e":
            count_e += 1
        elif i.lower() == "i":
            count_i += 1
        elif i.lower() == "o":
            count_o += 1
        elif i.lower() == "u":
            count_u += 1
    print(f"""In your statement, there were {count_a} a's, {count_e} e's,
          {count_i} i's, {count_o} o's, and {count_u} u's.""")
    
count_vowels()
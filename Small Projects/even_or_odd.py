print("Even or Odd?")

def even_odd():
    number = int(input("Enter a number: "))
    if number % 2 == 0:
        print(f"The number {number} is even.")
    else:
        print(f"The number {number} is odd.")
        
even_odd()
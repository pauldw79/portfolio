from random import randint

def guessing_game():
    number = randint(1, 100)
    print(f"Guessing Game\nGuess the number between 1 and 100.")
    guess = int(input("Your guess: "))
    while True:
        if number == guess:
            print("Good guess!")
            break
        elif guess < number:
            guess = int(input("Too low! Guess again! "))
        elif guess > number:
            guess = int(input("Too high! Guess again! "))

guessing_game()
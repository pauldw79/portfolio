from wordlist import WordList

class Hangman:
    
    def __init__(self):
        pass
    
    # Choose a random word from wordlist
    def guess_word(self):
        wordlist = WordList("wordlist.txt")
        random_word = wordlist.get_random_word()
        return random_word
    
    # Create the initial underscore display
    def create_display(self, word):
        display = ""
        for letter in word:
            display += "_ "
        return display.strip()
    
    # Update display with correctly guessed letters and hangman drawing
    def update_display(self, word, display, guess, incorrect_guesses):
        new_display = ""
        for i in range(len(word)):
            if word[i] == guess:
                new_display += guess + " "
            else:
                new_display += display[i * 2] + " "
        if guess not in word:
            incorrect_guesses += 1
            new_display += "\n"
            if incorrect_guesses == 1:
                new_display += "  O\n"
            elif incorrect_guesses == 2:
                new_display += "  O\n /|\n"
            elif incorrect_guesses == 3:
                new_display += "  O\n /|\\\n"
            elif incorrect_guesses == 4:
                new_display += "  O\n /|\\\n /\n"
            elif incorrect_guesses == 5:
                new_display += "  O\n /|\\\n / \\\n"
        return new_display.strip(), incorrect_guesses
    
    # Main game
    def hangman(self):
        word = self.guess_word()
        display = self.create_display(word)
        guesses = []
        incorrect_guesses = 0
        max_incorrect_guesses = 5
        
        while "_" in display and incorrect_guesses < max_incorrect_guesses:
            print(display)
            print(f"Incorrect guesses: {incorrect_guesses}")
            guess = input("Guess a letter: ").lower()
            if guess in guesses:
                print("You already guessed that letter!")
            elif guess in word:
                display, incorrect_guesses = self.update_display(word, display, guess, incorrect_guesses)
                print("Correct!")
            else:
                display, incorrect_guesses = self.update_display(word, display, guess, incorrect_guesses)
                print("Incorrect!")
            guesses.append(guess)
            
        if "_" not in display:
            print(f"You won! You guessed the word: {word}")
        else:
            print(f"  O\n /|\\\n / \\\nYou lost! The word was: {word}\n  GAME OVER")

if __name__ == "__main__":
    # Create an instance of the Hangman class and start the game
    game = Hangman()
    game.hangman()
            

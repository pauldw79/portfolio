print("Hello World!")

hello = "Hello World!"
print(hello)

def hello_world():
    print("Hello World!")
    
hello_world()

class HelloWorld:
    def hello_world(self):
        print("Hello World!")
    

hello = HelloWorld()
hello.hello_world()

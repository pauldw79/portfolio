def largest_number():
    print("Enter some numbers and I'll return the largest.")
    number = []
    number.append(int(input("Number: ")))
    while True:
        again = input("Would you like to add another number? ")
        if again == "yes":
            number.append(int(input("Number: ")))
        else:
            break
    largest = max(number)
    print(f"The largest number is {largest}.")
    
largest_number()
    
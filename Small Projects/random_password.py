import random

def password():
    print("Random Password Generator")
    number = int(input("How many characters? "))
    options = ["q", "w", "e", "r", "t", "y", "u", "i", "o", "p",
               "a", "s", "d", "f", "g", "h", "j", "k", "l",
               "z", "c", "v", "b", "n", "m",
               "1", "2", "3", "4", "5", "6", "7", "8", "9", "0",
               "Q", "W", "E", "R", "T", "Y", "U", "I", "O", "P",
               "A", "S", "D", "F", "G", "H", "J", "K", "L",
               "Z", "C", "V", "B", "N", "M",
               "!", "@", "#", "$", "%", "^", "&", "*", "?"]
    random.shuffle(options)
    p_word = []
    p_word = [options.pop() for i in range(1, number + 1)]
    p_word_str = "".join(p_word)
    print(f"Your password is: {p_word_str}")
    
password()
def add_numbers():
    print("Add 2 Numbers")
    num_1 = int(input("Enter the first number: "))
    num_2 = int(input("Enter the second number: "))
    print(f"The numbers you want to add are {num_1} and {num_2}.")
    result = num_1 + num_2
    print(f"{num_1} + {num_2} = {result}")
    
add_numbers()
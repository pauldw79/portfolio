import random

class WordList:
    # Generate a random word from a large file of words.
    def __init__(self, filename):
        with open("wordlist.txt", "r") as f:
            self.words = [word.strip() for word in f if len(word.strip()) <= 8]
    
    def get_random_word(self):
        return random.choice(self.words)

    
